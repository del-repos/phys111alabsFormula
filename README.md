# phys111alabsFormula
If you know Golang, you can speed through lab calculations. 

The repo only has the setup for the following labs for (PHYS111):
+ 127
+ 6a1
+ 125
+ 126
+ 114
+ 107

_**Note**: that to use this functions, one may have to take a further dive into the code for more manipulation with them._


```
git clone https://github.com/AOrps/phys111alabsFormula.git

cd phys111alabsFormula

go run main.go
```

