package main

// File was formerly labCalcs.go

import (
	"fmt"
	"math"
)

// GRAVITY --> just a const
const GRAVITY float64 = 9.81

func main() {
	var option string
	fmt.Println("===================================================")
	fmt.Println("Type [lab_num], then press [ENTER]")
	fmt.Println("Examples:\n\t> 114\n\t> 126")
	fmt.Println("")
	fmt.Println("To EXIT, Press [X] Then [ENTER}")
	fmt.Println("===================================================")
	printListLabs()

	for {
		fmt.Print("\n> ")

		fmt.Scanf("%s", &option)
		if option == "127" {
			lab127()
		}
		if option == "6a1" {
			enter6a1()
		}
		if option == "125" {
			lab125()
		}
		if option == "126" {
			lab126()
		}
		if option == "114" {
			lab114()
		}
		if option == "107" {
			lab107()
		}
		if option == "X" {
			break
		}

	}
	// enter6a1()
	// pernt_diff_calc(5.539,5.537)
}

// printListLabs -> prints the list of available labs to use on this
func printListLabs() {
	fmt.Println("+ 127\t\n+ 6a1\t\n+ 125\t\n+ 126\t\n+ 114\t\n+ 107")
}

func p(a string) {
	fmt.Println(a)
}

func p2(str string, val float64) {
	fmt.Println(str, val)
}

func stdDev(lst []float64, avrg float64) float64 {
	var proxyList []float64
	for _, val := range lst {
		proxy := float64(math.Abs(float64(val) - float64(avrg)))
		proxyList = appendToList(proxyList, proxy)
	}

	var stepList []float64
	for _, val := range proxyList {
		newVal := math.Pow(float64(val), 2)
		stepList = appendToList(stepList, float64(newVal))
	}

	sumOfSquared := float64(sum(stepList))

	standardDeviation := math.Sqrt((sumOfSquared / float64(len(lst)-1)))

	return float64(standardDeviation)
}

func recAppend(lst []float64, numAt int, size int) []float64 {
	if numAt >= size {
		return lst
	}
	return recAppend(lst, numAt+1, size)
}

func appendToList(lst []float64, newVal float64) []float64 {
	lst = append(lst, newVal)
	return lst
}

func average(beat []float64) float64 {
	avr := sum(beat) / float64(len(beat))
	return avr
}

func sum(arr []float64) float64 {
	var summa float64 = 0.0

	for _, i := range arr {
		summa += float64(i)
	}

	return float64(summa)
}

func makeLstFromPos(lst []float64, post int) []float64 {
	var newList []float64

	for key, value := range lst {
		if key == post {
			newList = appendToList(newList, value)
		}
	}

	return newList
}

func enter6a1() {

	p1 := []float64{0.000, 0.000, 0.601}
	p2 := []float64{0.046, 0.319, 0.601}
	p3 := []float64{0.662, 1.188, 0.615}
	p4 := []float64{1.196, 1.614, 0.631}

	posChanges2(p1, p2, .28974, .612, 0, 1)
	posChanges2(p2, p3, .28974, .612, 0, 1)
	posChanges2(p3, p4, .28974, .612, 0, 1)
	posChanges2(p1, p4, .28974, .612, 0, 1)

	p1a := []float64{0.000, 0.024, 0.376}
	p2a := []float64{0.002, 0.038, 0.376}
	p3a := []float64{0.003, 0.048, 0.391}
	p4a := []float64{0.005, 0.063, 0.361}

	posChanges2(p1a, p2a, .28974, .612, 5, 2)
	posChanges2(p2a, p3a, .28974, .612, 5, 2)
	posChanges2(p3a, p4a, .28974, .612, 5, 2)
	posChanges2(p1a, p4a, .28974, .612, 5, 2)

}

func setPrecision(val float64, precision int) float64 {
	precisionVal := 1.0 * math.Pow(10, float64(precision))
	valuePrecised := (val * precisionVal) / precisionVal
	return valuePrecised
}

func delta(final float64, initial float64) float64 {
	return final - initial
}

func percentDiffCalc(val1 float64, val2 float64) float64 {
	numerator := math.Abs((val1 - val2))
	denominator := ((val1 + val2) / 2)
	cherry := 100.00
	final := (numerator / denominator) * cherry
	return final
}

func averageTimeFor(amt int) []float64 {
	var averageTime []float64

	for i := 0; i < 5; i++ {
		timeSet := data("for Time", amt)
		fmt.Println("Average of Time of 50 Revolutions", average(timeSet))
		averageTime = appendToList(averageTime, average(timeSet))
	}
	return averageTime
}

func data(reason string, amt int) []float64 {
	var dataset []float64

	for i := 0; i < amt; {
		var val float64
		fmt.Printf("Data Number %d for %s\n", i+1, reason)
		fmt.Scanf("%f", &val)
		dataset = appendToList(dataset, val)
		i++
	}
	return dataset
}

func datapoints(reason string) []float64 {
	var dataset []float64
	var amtInt int
	fmt.Printf("Enter amount of data for %s:", reason)
	fmt.Scanf("%d", &amtInt)

	for i := 0; i < amtInt; {
		var val float64
		fmt.Printf("Data Number %d\n", i+1)
		fmt.Scanf("%f", &val)
		dataset = appendToList(dataset, val)
		i++
	}
	return dataset
}

func lab125() {
	displacement := datapoints("Displacement")
	velocity := datapoints("Velocity")

	mass := .33573
	k := 0.98

	for i := 0; i < len(displacement); {
		me125lab(mass, k, displacement[i], velocity[i])
		i++
	}
}

func me125lab(mass, k, x, velocity float64) {
	KE := kenergy(mass, velocity)
	PE := peWhileOCC(k, x, mass)
	ME := kenergy(mass, velocity) + peWhileOCC(k, x, mass)
	p("KE      PE      ME")
	fmt.Println(KE, PE, ME)
}

func peWhileOCC(k, x, m float64) float64 {
	leftSide := .5 * k * math.Pow(x, 2)
	rightSide := (math.Pow((m * GRAVITY), 2)) / (2 * k)
	equation := leftSide + rightSide
	return equation
}

func kenergy(mass float64, velocity float64) float64 {
	equation := .5 * mass * math.Pow(velocity, 2)
	return equation
}

func posChanges2(lst1 []float64, lst2 []float64, mass float64, force float64, angle float64, order int) {
	s := delta(lst2[0], lst1[0]) // s = x(>f) - x(>i)
	W := force * s * math.Cos(angle)
	KE1 := .5 * mass * math.Pow(lst1[1], 2)
	KE2 := .5 * mass * math.Pow(lst2[1], 2)
	dKE := delta(KE2, KE1)
	dHeight := s * math.Sin(angle)
	dPE := mass * GRAVITY * dHeight
	dEnergy := dPE + dKE
	percentDiff := percentDiffCalc(W, dKE)

	if order == 1 {
		p2("S:", s)
		p2("W:", W)
		p2("KE(i):", KE1)
		p2("KE(f):", KE2)
		p2("dKE:", dKE)
		p2("% difference", percentDiff)
		p("")
	} else {
		p2("S:", s)
		p2("W:", W)
		p2("dKE:", dKE)
		p2("dPE:", dPE)
		p2("dEnergy:", dEnergy)
		p2("% difference", percentDiff)
		p("")
	}
}

func linearMomentum(mass, velocity float64) float64 {
	return mass * velocity
}

func impulseLab126(p1, p2 float64) float64 {
	return delta(p1, p2)
}

func dTime126(dJimpulsi, Favg float64) float64 {
	equation := dJimpulsi / Favg
	return equation
}

func total(val1, val2 float64) float64 {
	return val1 + val2
}

func divideEach(dataset []float64, denominator float64) []float64 {
	var newList []float64

	for _, val := range dataset {
		value := val / denominator
		newList = appendToList(newList, value)
	}
	return newList
}

func force126(mass, RPS, radius float64) float64 {
	forceComputed := 4 * math.Pow(math.Pi, 2) * mass * math.Pow(RPS, 2) * radius
	return forceComputed
}

func lab126_2(amt int) {
	var totalPBefore []float64
	var totalPAfter []float64
	var KEBefore []float64
	var KEAfter []float64

	m1 := data("M1", amt)
	m2 := data("M2", amt)
	vi := data("Vi", amt)
	vf := data("vf", amt)

	for i := 0; i < amt; {
		totalBefore := total(linearMomentum(m1[i], vi[i]), linearMomentum(m2[i], vi[i]))
		totalPBefore = appendToList(totalPBefore, totalBefore)

		totalAfter := total(linearMomentum(m1[i], vf[i]), linearMomentum(m2[i], vf[i]))
		totalPAfter = appendToList(totalPAfter, totalAfter)

		totalBeforeKE := total(kenergy(m1[i], vi[i]), kenergy(m2[i], vi[i]))
		KEBefore = appendToList(KEBefore, totalBeforeKE)

		totalAfterKE := total(kenergy(m1[i], vf[i]), kenergy(m2[i], vf[i]))
		KEAfter = appendToList(KEAfter, totalAfterKE)
		i++
	}

	fmt.Println("Total P before", totalPBefore)
	fmt.Println("Total P After", totalPAfter)
	fmt.Print("Total KE Before", KEBefore)
	fmt.Println("Total KE After", KEAfter)

}

func lab126() {
	var trialNum int = 3
	var p1KE, p2KE, totalPKE, p1fKE, p2fKE, totalPfKE, perDiffKE []float64

	m1 := data("M1", trialNum)
	m2 := data("M2", trialNum)
	v1i := data("V1(initial)", trialNum)
	v1f := data("V1(final)", trialNum)
	v2i := data("V2(initial)", trialNum)
	v2f := data("V2(final)", trialNum)

	fmt.Println("Kinetic Energy- Data Table 1")
	for j := 0; j < trialNum; j++ {
		val := kenergy(m1[j], v1i[j])
		p1KE = appendToList(p1KE, val)
		val2 := kenergy(m2[j], v2i[j])
		p2KE = appendToList(p2KE, val2)

		totalPKE = appendToList(totalPKE, val+val2)

		val3 := kenergy(m1[j], v1f[j])
		p1fKE = appendToList(p1fKE, val3)
		val4 := kenergy(m2[j], v2f[j])
		p2fKE = appendToList(p2fKE, val4)

		totalPfKE = appendToList(totalPfKE, val3+val4)

		perDiffKE = appendToList(perDiffKE, percentDiffCalc(totalPKE[j], totalPfKE[j]))
	}
	fmt.Println("p1", p1KE)
	fmt.Println("p2", p2KE)
	fmt.Println("total P", totalPKE)
	fmt.Println("p1f", p1fKE)
	fmt.Println("p2f", p2fKE)
	fmt.Println("total_pf", totalPfKE)
	fmt.Println("per_diff", perDiffKE)

	lab126_2(trialNum)
}

func lab114() {
	Trials := 5
	var forceComputed []float64
	var pDiff []float64

	forMass := data("Mass", Trials)
	forRadius := data("Radius", Trials)
	forceMeasured := data("Force Measured", Trials)

	averageTimes := averageTimeFor(3)
	RPS := divideEach(averageTimes, 50)

	for i := 0; i < Trials; i++ {
		forceCompute := force126(forMass[i], RPS[i], forRadius[i])
		forceComputed = appendToList(forceComputed, forceCompute)
	}

	for j := 0; j < Trials; j++ {
		percentDiffVal := percentDiffCalc(forceComputed[j], forceMeasured[j])
		pDiff = appendToList(pDiff, percentDiffVal)
	}

	fmt.Println(averageTimes)
	fmt.Println(RPS)
	fmt.Println(forceComputed)
	fmt.Println(pDiff)
	fmt.Println("")
	fmt.Println("")
}
func getTension(mass, acc float64) float64 {
	equation := mass * (GRAVITY - acc)
	return equation
}

func lab127() {
	var tension, torque, angularAcceleration, TRInertia []float64
	var m float64 = .1
	// radii := []float64{.112,.04832,.171,.055}
	linearAcceleration := data("acc", 12)

	for _, val := range linearAcceleration {
		valT := getTension(m, val)

		tension = append(tension, valT)
	}

	fmt.Println(tension)
	fmt.Println(torque)
	fmt.Println(angularAcceleration)
	fmt.Println(TRInertia)

}

func lab107() {
	var newLst []float64
	lst := []float64{0.0027350427350427346, 0.010799654983141222, 0.000849055124284482, 0.007882066964635772}
	for i := range lst {
		val := lst[i] * 1000
		newLst = appendToList(newLst, val)
	}

	fmt.Println("Archimedes Principle:", newLst)

}

func getR(Rx float64, Ry float64) float64 {
	return math.Sqrt((math.Pow(Rx, 2)) + (math.Pow(Ry, 2)))
}

func getAngle(Ry float64, Rx float64) float64 {
	return math.Atan((Ry / Rx))
}
